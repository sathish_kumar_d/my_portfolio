from django.shortcuts import render, get_object_or_404
from .models import Blog
from django.http import Http404

def blog(request):
    blogs = Blog.objects.all()
    return render(request, 'my_blogs/blog.html', {'blogs':blogs})

def detail(request, blog_id):
    try:
        blog = get_object_or_404(Blog, pk = blog_id)
        return render(request, 'my_blogs/detail.html', {'blog':blog})
    except Http404:
        return render(request, 'my_blogs/no_detail.html')